TARGET = opensteerdemo


INCLUDEPATH += include

HEADERS = include/OpenSteer/*.h
SOURCES = src/*.cpp src/*.c plugins/*.cpp

LIBS = -lglut -lGLU -lGL
DEFINES += OPENSTEER USEOpenGL
